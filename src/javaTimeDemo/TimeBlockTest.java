package javaTimeDemo;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class TimeBlockTest {

	public static void main(String []args) {
		
		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.MONTH, 02);
		calendar.set(Calendar.DATE, 20);
		calendar.set(Calendar.YEAR, 2021);

		Date date = calendar.getTime();
				
		LocalDateTime colomboTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Asia/Colombo"));
		LocalDateTime parisTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Europe/Paris"));

		print("colomboTime : " + colomboTime.toString());
		print("parisTime : " + parisTime.toString());
		
	}
	
	public static void print(String text) {
		System.out.println("");
		System.out.println(text);
		System.out.println("");
	}
}
