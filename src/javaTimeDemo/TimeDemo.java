package javaTimeDemo;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

public class TimeDemo {

	public static void main(String []args) {
		
		/* LocalDate */
		
		LocalDate localDate = LocalDate.of(2021, 03, 20);
		print("localDate : " + localDate);
		
		LocalDate localDateParsed = LocalDate.parse("2021-03-20");
		print("localDateParsed : " + localDateParsed);
		
		String localDateFormatted = localDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		print("localDateFormatted : " + localDateFormatted);
		
		//LocalDate dateInNextWeek = localDate.plus(7, ChronoUnit.DAYS);
		//LocalDate dateInNextWeek = localDate.plus(Period.ofDays(7));
		LocalDate dateInNextWeek = localDate.plusDays(7);
		print("dateInNextWeek : " + dateInNextWeek);
		
		//int dateDifference = Period.between(localDate, dateInNextWeek).getDays();
		long dateDifference = ChronoUnit.DAYS.between(localDate, dateInNextWeek);
		print("dateDifference : " + dateDifference);
		
		LocalDate startDate = LocalDate.parse("2021-03-20");
		LocalDate endDate = LocalDate.parse("2021-04-20");
		boolean isBeforeDate = startDate.isBefore(endDate);
		boolean isAfterDate = startDate.isAfter(endDate);
		print("startDate : " + startDate + " is-before, endDate : " + endDate + " ? " + isBeforeDate);
		print("startDate : " + startDate + " is-after, endDate : " + endDate + " ? " + isAfterDate);
		
		LocalDate firstDayOfMonth = localDate.with(TemporalAdjusters.firstDayOfMonth());
		print("firstDayOfMonth : " + firstDayOfMonth);
		
		LocalDate lastDayOfMonth = localDate.with(TemporalAdjusters.lastDayOfMonth());
		print("lastDayOfMonth : " + lastDayOfMonth);
		
		
		/* LocalTime */
		
		LocalTime localTime = LocalTime.of(9, 30, 45, 50);
		print("localTime : " + localTime);
		
		LocalTime localTimeParsed = LocalTime.parse("09:30");
		print("localTimeParsed : " + localTimeParsed);
		
		String localTimeformatted = localTime.format(DateTimeFormatter.ofPattern("hh:mm:ss"));
		print("localTimeformatted : " + localTimeformatted);

		//LocalTime timeAtNextInterval = localTime.plus(Duration.ofSeconds(30));		
		//LocalTime timeAtNextInterval = localTime.plus(30, ChronoUnit.SECONDS);
		LocalTime timeAtNextInterval = localTime.plusSeconds(30);
		print("timeAtNextInterval : " + timeAtNextInterval);
		
		long intervalDiff = ChronoUnit.SECONDS.between(localTime, timeAtNextInterval);
		print("intervalDiff : " + intervalDiff);
		
		LocalTime startTime = LocalTime.parse("09:30");
		LocalTime endTime = LocalTime.parse("07:30");
		boolean isBefore = startTime.isBefore(endTime);
		boolean isAfter = startTime.isAfter(endTime);
		print("startTime : " + startTime + " is-before, endTime : " + endTime + " ? " +  isBefore);
		print("startTime : " + startTime + " is-after, endTime : " + endTime + " ? " + isAfter);

		
		/* LocalDateTime */
		
		LocalDateTime localDateTime = LocalDateTime.of(2021, Month.MARCH, 27, 9, 30);
		print("localDateTime : " + localDateTime);
		
		LocalDateTime localDateTimeParsed = LocalDateTime.parse("2021-03-20T09:30:00");
		print("localDateTimeParsed : " + localDateTimeParsed);
		
		String localDateTimeFormatted = localDateTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss"));
		print("localDateTimeFormatted : " + localDateTimeFormatted);
		
		String localISODate = localDateTime.format(DateTimeFormatter.ISO_DATE);
		print("localISODate : " + localISODate);
		
		String ukDateTimeFormat = localDateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(Locale.UK));
		print("ukDateTimeFormat : " + ukDateTimeFormat);
		
		LocalDateTime dateInNextYear = localDateTime.plus(1, ChronoUnit.YEARS);
		DayOfWeek dayOfWeek = dateInNextYear.getDayOfWeek();
		print("dateInNextYear : " + dateInNextYear + " / dayOfWeek : " + dayOfWeek );
		
		/* ZonedDateTime */
		
		ZonedDateTime zonedDateTimeNow = ZonedDateTime.now();
		print("zonedDateTimeNow : " + zonedDateTimeNow);
		
		ZonedDateTime zonedDateTimeDenver = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("America/Denver"));
		print("zonedDateTimeDenver : " + zonedDateTimeDenver);
		
		ZoneId zoneBerlin = ZoneId.of("Europe/Berlin");
		ZonedDateTime currentZonedDateTime = ZonedDateTime.now(zoneBerlin);
		print("ZonedDateTimeCurrent : " + currentZonedDateTime);

		ZoneId zoneId = ZoneId.of("Europe/Paris");
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);
		print("zonedDateTime : " + zonedDateTime);
		
		boolean isBeforeTime = zonedDateTime.isBefore(zonedDateTimeNow);
		print("zonedDateTime  : " + zonedDateTime + " is-before, zonedDateTimeNow : "+ zonedDateTimeNow + " ? " + isBeforeTime);
		
		/* All Zones */
		
		Set<String> zoneIds = ZoneId.getAvailableZoneIds();
		
		for(String zone : zoneIds) {
			print("zone : " + zone);
		}

		/* ZoneOffset */
		
		ZoneOffset offset = ZoneOffset.of("-07:00");
		OffsetDateTime offsetDateTime = OffsetDateTime.of(LocalDateTime.of(2021, Month.MARCH, 27, 9, 30), offset);
		print("offsetDateTime : " + offsetDateTime);

		
		/* Legacy to java.time */
		
	    Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MONTH, 02);
        calendar.set(Calendar.DATE, 20);
        calendar.set(Calendar.YEAR, 2021);

        Date date = calendar.getTime();
		
		LocalDateTime colomboTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Asia/Colombo"));
		print("colomboTime : " + colomboTime.toString());
		
		LocalDateTime parisTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("Europe/Paris"));
		print("parisTime : " + parisTime.toString());
		
		/* Get Time Now */
		
		Instant nowInstant = Instant.now();
		print("nowInstant : " + nowInstant);
		
		LocalDateTime localDateTimeNow = LocalDateTime.now();
		print("localDateTimeNow : " + localDateTimeNow);

		/* OffsetDateTime vs ZonedDateTime */
		
		ZoneId zone = ZoneId.of("Europe/Berlin");
		ZonedDateTime zonedDateTimeBerlin = ZonedDateTime.now(zone);
		print("zonedDateTimeBerlin : " + zonedDateTimeBerlin);
		
		ZoneOffset zoneOffSetManual = ZoneOffset.of("+02:00");
		OffsetDateTime offsetDateTimeManual = OffsetDateTime.now(zoneOffSetManual);
		print("offsetDateTimeManual : " + offsetDateTimeManual);
		
		/* EPOCH */
		
		long epochSeconds = zonedDateTimeBerlin.toEpochSecond();
		print("epochSeconds : " + epochSeconds);
	}
	
	public static void print(String text) {
		System.out.println(text);
	}
}
