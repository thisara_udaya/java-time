# Java8 Time Package - Demo #

This is a demonstration of java8 time package class capabilities.

### Setup ###

This is a standalone Java project.

1. Clone project
2. Run the TimeDemo : java TimeDemo
3. If independant blocks executed use, copy each block in to TimeBlockTest and execute the class : java TimeBlockTest

Video Reference : https://www.youtube.com/watch?v=poO-_V339mY 
Java Time Blog : 
- https://mytechblogs.com/2021/04/09/java-time/
- https://mytechblogs.medium.com/java-time-7254838dedcb
